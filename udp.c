#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include "udp.h"


int udp_open(void)
{
    int sockfd;
    struct sockaddr_in server;

    sockfd = socket(AF_INET, SOCK_DGRAM | SOCK_CLOEXEC, 0);
    if (sockfd < 0) {
        return -1;
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(22400);
    server.sin_addr.s_addr = INADDR_ANY;

    if (bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0) {
       return -1;
    }
    return sockfd;
}

ssize_t udp_read(int sockfd, void *buf, size_t count, struct sockaddr *src_addr, socklen_t *addrlen)
{
    assert(buf);
    assert(src_addr);
    assert(addrlen);

    return recvfrom(sockfd, buf, count, 0, (struct sockaddr *) src_addr, addrlen);
}

ssize_t udp_write(int sockfd, const void *buf, size_t len, const struct sockaddr *dest_addr, socklen_t addrlen)
{
    assert(buf);
    assert(dest_addr);

    return sendto(sockfd, buf, len, 0, (struct sockaddr *) dest_addr, addrlen);
}

void udp_close(int sockfd)
{
    close(sockfd);
}

