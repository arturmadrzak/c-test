#ifndef _PROTO_H
#define _PROTO_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/* Shortest signals are 2 bytes long */
#define MIN_REQUEST_LEN (2)
/* How much spce do we need to form a response */
#define MAX_RESPONSE_LEN (10)

#define MAX_RELAY_ID (1)

enum {
    STATUS_ERROR = -1,
    STATUS_NACK,
    STATUS_ACK,
    STATUS_REQUEST,
};

enum {
    SIGNAL_ACK = 0x11,
    SIGNAL_SET_RELAY = 0x21,
    SIGNAL_GET_RELAY = 0x31,
    SIGNAL_RELAY_STATE = 0x41,
    SIGNAL_QUIT = 0x51,
};

struct frame {
    uint8_t code;
    uint8_t seq_no;
    uint8_t payload[];
} __attribute__((packed));

struct proto;

struct proto* proto_alloc(void);
void proto_free(struct proto* p);

/*
 * returns:
 * -1 - error
 * 0 - noack
 * 1 - ack */
int proto_handle(struct proto *p, const uint8_t *request, size_t reqlen);
int proto_relay_state(struct proto *p, uint8_t *response, size_t maxrsp);
int proto_relay_state_ack(struct proto *p, const uint8_t *request, size_t reqlen);
int proto_ack(struct proto *p, uint8_t *response, size_t maxrsp);

void proto_api_quit_cb(void);
void proto_api_prepare_state_cb(void);
void proto_api_set_relay_cb(uint8_t number, uint8_t value);
bool proto_api_get_relay_cb(uint8_t number);


#endif /* _PROTO_H */
