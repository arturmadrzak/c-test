#include <errno.h>
#include <netinet/ip.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "proto.h"
#include "udp.h"


static bool quit = false;

static void sig_handler(int signo)
{
    quit = true;
}

void proto_api_prepare_state_cb(void)
{
    printf("...Received Get State\n");
}

void proto_api_quit_cb(void)
{
    printf("...Quit\n");
    quit = true;
}

void proto_api_set_relay_cb(uint8_t number, uint8_t value)
{
    printf("...Relay %u: %s\n", number, value ? "Close" : "Open");
}

bool proto_api_get_relay_cb(uint8_t number)
{
    bool result = false;
    if (number == 0)
        result = true;

    printf("...Get relay %u: %s\n", number, result ? "Close" : "Open");
    return result;
}

int main() {
    struct proto *proto;
    struct sigaction sa;
    struct sockaddr_in client, server;
    socklen_t client_addr_size, namelen;
    int sockfd;
    uint8_t request[32], response[32];
    ssize_t reqlen, resplen;

    if (!(proto = proto_alloc())) {
        fprintf(stderr, "proto_alloc(): %s\n", strerror(errno));
        exit(-1);
    }

    sa.sa_handler = sig_handler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);

    if (sigaction(SIGINT, &sa, NULL) < 0) {
        fprintf(stderr, "sigaction(): %s\n", strerror(errno));
        exit(-1);
    }

    sockfd = udp_open();
    if (sockfd < 0) {
        fprintf(stderr, "udp_open(): %s\n", strerror(errno));
        exit(-1);
    }

    namelen = sizeof(server);
    if (getsockname(sockfd, (struct sockaddr *) &server, &namelen) < 0)
    {
       fprintf(stderr, "getsockname(): %s\n", strerror(errno));
       exit(-1);
    }

    printf("Emulator started at port %u\n", ntohs(server.sin_port));

    do {
        int status;
        client_addr_size = sizeof(client);
        /* assuming only one client */
        reqlen = udp_read(sockfd, request, sizeof(request), (struct sockaddr *)&client, &client_addr_size);
        if (reqlen < 0) {
            /* don't print error message when exit by Ctrl-C */
            if (errno != EINTR) {
               fprintf(stderr, "recvfrom(): %s\n", strerror(errno));
            }
            exit(-1);
        }

        status = proto_handle(proto, request, reqlen);
        if (status == STATUS_ERROR) {
            fprintf(stderr, "proto_handle(): %s\n", strerror(errno));
            exit(-1);
        }

        if (status == STATUS_ACK || status == STATUS_REQUEST) {
            /* Send ACK */
            resplen = proto_ack(proto, response, sizeof(response));
            if (udp_write(sockfd, response, resplen, (struct sockaddr *)&client, client_addr_size) < 0) {
                fprintf(stderr, "udp_write(): %s\n", strerror(errno));
                exit(-1);
            }
        }

        if (status == STATUS_REQUEST) {
            /* Send data if any. ACK for data is handled in proto_handle */
            if ((resplen = proto_relay_state(proto, response, sizeof(response))) > 0) {
                if (udp_write(sockfd, response, resplen, (struct sockaddr *)&client, client_addr_size) < 0) {
                    fprintf(stderr, "udp_write(): %s\n", strerror(errno));
                    exit(-1);
                }
                do {
                    reqlen = udp_read(sockfd, request, sizeof(request), (struct sockaddr *)&client, &client_addr_size);
                    if (reqlen < 0) {
                        /* don't print error message when exit by Ctrl-C */
                        if (errno != EINTR) {
                           fprintf(stderr, "recvfrom(): %s\n", strerror(errno));
                        }
                        exit(-1);
                    }
                } while(proto_relay_state_ack(proto, request, reqlen) != STATUS_ACK);
            }
        }
    } while(!quit);

    close(sockfd);
    exit(0);
}
