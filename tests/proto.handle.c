#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

static uint8_t request[32];
static struct proto *p;

Describe(proto_handle);

BeforeEach(proto_handle)
{
    p = proto_alloc();
}

AfterEach(proto_handle)
{
    proto_free(p);
}

Ensure(proto_handle, returns_error_on_invalid_params)
{
    int result;

    result = proto_handle(NULL, request, sizeof(request));
    assert_that(result, is_equal_to(-1));

    result = proto_handle(p, NULL, sizeof(request));
    assert_that(result, is_equal_to(-1));

    result = proto_handle(p, request, (MIN_REQUEST_LEN-1));
    assert_that(result, is_equal_to(-1));
}

Ensure(proto_handle, returns_nack_for_incoming_ack_frame)
{
    int result;
    uint8_t given[] = { SIGNAL_ACK, 0 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, returns_nack_for_incoming_relay_state)
{
    int result;
    uint8_t given[] = { SIGNAL_RELAY_STATE, 0x00, 0x01 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, returns_ack_for_quit)
{
    int result;
    uint8_t given[] = { SIGNAL_QUIT, 0x01 };

    expect(proto_api_quit_cb);
    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(1));
}

Ensure(proto_handle, returns_ack_for_get_relay)
{
    int result;
    uint8_t given[] = { SIGNAL_GET_RELAY, 0x01 };

    expect(proto_api_prepare_state_cb);

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(2));
}

Ensure(proto_handle, reutrns_nack_for_zero_length_request)
{
    int result;
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x00 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, reutrns_nack_for_length_mismatch)
{
    int result;
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x02, 0x00, 0x01, 0x01, 0x01, 0x00 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, reutrns_nack_if_more_than_2_relays_in_request)
{
    int result;
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x03, 0x00, 0x01, 0x01, 0x01, 0x02, 0x01 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, reutrns_nack_for_wrong_relay_number)
{
    int result;
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x02, 0x02, 0x01, 0x01, 0x01 };

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, reutrns_nack_for_wrong_relay_value)
{
    int result;
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x02, 0x00, 0x01, 0x01, 0x03 };

    /* Expect all correct values to be set. To be more defensive, checking
     * frame upfront can be implemented. Then we do not set any relay state
     * if any error occurs in frame. */
    expect(proto_api_set_relay_cb,
            when(number, is_equal_to(0)),
            when(value, is_equal_to(1)));

    result = proto_handle(p, given, sizeof(given));
    assert_that(result, is_equal_to(0));
}

Ensure(proto_handle, calls_set_relay_callback_for_both_relays)
{
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x02, 0x00, 0x01, 0x01, 0x01 };

    expect(proto_api_set_relay_cb,
            when(number, is_equal_to(0)),
            when(value, is_equal_to(1)));
    expect(proto_api_set_relay_cb,
            when(number, is_equal_to(1)),
            when(value, is_equal_to(1)));
    proto_handle(p, given, sizeof(given));
}

Ensure(proto_handle, calls_set_relay_callback_for_single_relay)
{
    uint8_t given[] = { SIGNAL_SET_RELAY, 0x01, 0x01, 0x01, 0x00 };

    expect(proto_api_set_relay_cb,
            when(number, is_equal_to(1)),
            when(value, is_equal_to(0)));
    proto_handle(p, given, sizeof(given));
}

Ensure(proto_handle, ignores_ack_with_wrong_seq_no)
{
    int result;
    uint8_t get_state[] = { 0x31, 0x00 };
    uint8_t wrong_ack[] = { 0x11, 0x00 };
    uint8_t response[32];

    expect(proto_api_prepare_state_cb);
    expect(proto_api_get_relay_cb,
            when(number, is_equal_to(0)),
            will_return(1));
    expect(proto_api_get_relay_cb,
            when(number, is_equal_to(1)),
            will_return(0));
    proto_handle(p, get_state, sizeof(get_state));
    proto_ack(p, response, sizeof(response));
    proto_relay_state(p, response, sizeof(response));

    result = proto_handle(p, wrong_ack, sizeof(wrong_ack));
    assert_that(result, is_equal_to(0));
}

