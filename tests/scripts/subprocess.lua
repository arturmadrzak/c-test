local SubProcess = {}
local posix = require("posix")

SubProcess.new = function(path, ...)
    local self = {}
    local process_path = path
    local p = ...
    local pid = nil

    self.start = function()
        pid = posix.fork()
        assert(pid ~= nil, "fork() failed")
        if pid == 0 then
            local ret, err = posix.execp(process_path, p)
            assert(ret ~= nil, "execp() failed")
            posix._exit(1)
            return err
        end
    end

    self.stop = function()
        assert(pid ~= nil, "process not started")
        posix.kill(pid)
    end

    self.get_pid = function()
        return pid
    end

    return self
end

return SubProcess
