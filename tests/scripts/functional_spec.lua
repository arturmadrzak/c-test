package.path = package.path .. ";./tests/scripts/?.lua"

require 'busted.runner'()
local socket = require("socket")
local subprocess = require("subprocess")
local sleep = require("posix").sleep

describe(
    "emulator", function()
        local udp
        local main
        before_each()(
            function()
                main = subprocess.new("./main")
                main.start()
                sleep(1)
                udp = socket.udp()
                assert(udp:setsockname("*",0))
                assert(udp:setpeername("localhost", 22400))
                assert(udp:settimeout(1))
            end
        )
        after_each()(
            function()
                pcall(main.stop)
            end
        )

        it(
            "relay signal sent from client", function()
                local relay_signal = string.char(0x21, 0x99, 0x01, 0x00, 0x01)
                local expected = string.char(0x11, 0x9a)
                udp:send(relay_signal)
                ack = udp:receive()
                assert.are.equal(expected, ack)
            end
        )

        it(
            "quit signal sent from client", function()
                local quit = string.char(0x51, 0x01)
                local open_relay_0 = string.char(0x21, 0x01, 0x01, 0x00, 0x00)
                local expected = string.char(0x11, 0x02)
                udp:send(quit)
                ack = udp:receive()
                assert.are.equal(expected, ack)

                udp:send(open_relay_0)
                ack, err = udp:receive()
                assert.are.equal(ack, nil)
                assert.is_not.equals(err, nil)
            end
        )

        it(
            "get state signal sent from client", function()
                local get_state = string.char(0x31, 0x9a)
                local expected_get_state_ack = string.char(0x11, 0x9b)
                local expected_relay_state = string.char(0x41, 0x9b, 0x01)
                local relay_state_ack = string.char(0x11, 0x9c)
                local ack
                local state

                udp:send(get_state)
                ack = udp:receive()
                assert.are.equal(expected_get_state_ack, ack)
                state = udp:receive()
                assert.are.equal(expected_relay_state, state)
                udp:send(relay_state_ack)
            end
        )

        it(
            "multiple requests", function()
                local open_relay_0 = string.char(0x21, 0x01, 0x01, 0x00, 0x00)
                local open_relay_0_ack = string.char(0x11, 0x02)
                local close_relay_1 = string.char(0x21, 0x02, 0x01, 0x01, 0x01)
                local close_relay_1_ack = string.char(0x11,0x03)
                local open_both = string.char(0x21, 0x03, 0x02, 0x00, 0x00, 0x01, 0x00)
                local open_both_ack = string.char(0x11,0x04)
                local close_both = string.char(0x21, 0x04, 0x02, 0x00, 0x01, 0x01, 0x01)
                local close_both_ack = string.char(0x11,0x05)
                local get_relay_state = string.char(0x31, 0x05)
                local get_relay_state_ack = string.char(0x11, 0x06)
                local expected_relay_state = string.char(0x41, 0x06, 0x01)
                local ack_relay_state = string.char(0x11, 0x07)
                local quit = string.char(0x51, 0x07)
                local quit_ack = string.char(0x11, 0x08)
                local ack
                local state

                udp:send(open_relay_0)
                ack = udp:receive()
                assert.are.equal(ack, open_relay_0_ack)

                udp:send(close_relay_1)
                ack = udp:receive()
                assert.are.equal(ack, close_relay_1_ack)

                udp:send(open_both)
                ack = udp:receive()
                assert.are.equal(ack, open_both_ack)

                udp:send(close_both)
                ack = udp:receive()
                assert.are.equal(ack, close_both_ack)

                udp:send(get_relay_state)
                ack = udp:receive()
                assert.are.equal(ack, get_relay_state_ack)

                state = udp:receive()
                assert.are.equal(state, expected_relay_state)
                udp:send(ack_relay_state)

                udp:send(quit)
                ack = udp:receive()
                assert.are.equal(ack, quit_ack)



            end
        )

    end
)
