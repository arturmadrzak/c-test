#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include "udp.h"


int udp_open(void)
{
    return (int)mock();
}

ssize_t udp_read(int sockfd, void *buf, size_t count, struct sockaddr *src_addr, socklen_t *addrlen)
{
    return (ssize_t)mock(sockfd, buf, count, src_addr, addrlen);
}

ssize_t udp_write(int sockfd, const void *buf, size_t len, const struct sockaddr *dest_addr, socklen_t addrlen)
{
    return (ssize_t)mock(sockfd, buf, len, dest_addr, addrlen);
}

void udp_close(int sockfd)
{
    mock(sockfd);
}

