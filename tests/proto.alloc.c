#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

Describe(proto_alloc);

BeforeEach(proto_alloc)
{
}

AfterEach(proto_alloc)
{
}

Ensure(proto_alloc, allocates_initialized_memory)
{
    struct proto *p = proto_alloc();
    assert_that(p, is_not_equal_to(NULL));
    assert_that((*(uint8_t*)p), is_equal_to(0));
}

Ensure(proto_alloc, free_doesnt_segfault_on_null_ptr)
{
    // a bit redundant, as most modern c libraries
    // handle it correctly
    proto_free(NULL);
}
