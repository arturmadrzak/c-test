#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

void proto_api_prepare_state_cb(void)
{
    mock();
}

void proto_api_quit_cb(void)
{
    mock();
}

void proto_api_set_relay_cb(uint8_t number, uint8_t value)
{
    mock(number, value);
}

bool proto_api_get_relay_cb(uint8_t number)
{
    return (bool)mock(number);
}
