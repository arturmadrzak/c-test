#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

struct proto* proto_alloc(void)
{
    return (struct proto*)mock();
}

int proto_handle(struct proto *p, const uint8_t *request, size_t reqlen)
{
    return (int)mock(p, request, reqlen);
}

int proto_relay_state(struct proto *p, uint8_t *response, size_t maxrsp)
{
    return (int)mock(p, response, maxrsp);
}

int proto_relay_state_ack(struct proto *p, const uint8_t *request, size_t reqlen)
{
    return (int)mock(p, request, reqlen);
}

int proto_ack(struct proto *p, uint8_t *response, size_t maxrsp)
{
    return (int)mock(p, response, maxrsp);
}
