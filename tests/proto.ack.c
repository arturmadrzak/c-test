#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

static uint8_t response[32];
static struct proto *p;

Describe(proto_ack);

BeforeEach(proto_ack)
{
    p = proto_alloc();
}

AfterEach(proto_ack)
{
    proto_free(p);
}

Ensure(proto_ack, returns_error_on_invalid_params)
{
    int result;

    result = proto_ack(NULL, response, sizeof(response));
    assert_that(result, is_equal_to(-1));

    result = proto_ack(p, NULL, sizeof(response));
    assert_that(result, is_equal_to(-1));

    result = proto_ack(p, response, 1);
    assert_that(result, is_equal_to(-1));
}

Ensure(proto_ack, returns_ack_for_quit)
{
    int result;
    uint8_t given[] = { SIGNAL_QUIT, 0xa9 };
    uint8_t expected[] = { SIGNAL_ACK, 0xaa };

    expect(proto_api_quit_cb);
    proto_handle(p, given, sizeof(given));
    result = proto_ack(p, response, sizeof(response));
    assert_that(result, is_equal_to(2));
    assert_that(response, is_equal_to_contents_of(expected, 2));
}


