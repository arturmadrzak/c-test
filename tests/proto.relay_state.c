#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include "proto.h"

static uint8_t response[32];
static struct proto *p;

Describe(proto_relay_state);

BeforeEach(proto_relay_state)
{
    p = proto_alloc();
}

AfterEach(proto_relay_state)
{
    proto_free(p);
}

Ensure(proto_relay_state, returns_error_on_invalid_params)
{
    int result;

    result = proto_relay_state(NULL, response, sizeof(response));
    assert_that(result, is_equal_to(-1));

    result = proto_relay_state(p, NULL, sizeof(response));
    assert_that(result, is_equal_to(-1));

    result = proto_relay_state(p, response, 2);
    assert_that(result, is_equal_to(-1));
}

Ensure(proto_relay_state, returns_correct_frame)
{
    int result;
    uint8_t expected[] = { 0x41, 0x00, 0x01 };

    expect(proto_api_get_relay_cb,
            when(number, is_equal_to(0)),
            will_return(1));
    expect(proto_api_get_relay_cb,
            when(number, is_equal_to(1)),
            will_return(0));

    result = proto_relay_state(p, response, sizeof(response));
    assert_that(result, is_equal_to(3));
    assert_that(response, is_equal_to_contents_of(expected, sizeof(expected)));
}

