#include <stdlib.h>
#include "proto.h"

#include <stdio.h>

struct proto {
    uint8_t seq_no;
    bool get;
};

struct proto* proto_alloc(void)
{
    return calloc(1, sizeof(struct proto));
}

void proto_free(struct proto* p)
{
    if (p)
        free(p);
}

static int get_relay_signal(struct proto *p)
{
    proto_api_prepare_state_cb();
    return STATUS_REQUEST;
}

static int set_relay_signal(struct proto *p, const uint8_t *payload, size_t len)
{
    uint8_t count = payload[0];
    const uint8_t *rinfo = &payload[1];
    int i;
    if (!count || count > 2 || (1+count*2 != len))
        return STATUS_NACK;

    for(i = 0; i < count; i++, rinfo += 2)
    {
        uint8_t number = rinfo[0];
        uint8_t value = rinfo[1];
        if (number > MAX_RELAY_ID)
            return STATUS_NACK;
        if (value & ~0x01)
            return STATUS_NACK;
        proto_api_set_relay_cb(number, value);
    }
    return STATUS_ACK;
}

static int quit_signal(struct proto *p)
{
    proto_api_quit_cb();
    return STATUS_ACK;
}

int proto_handle(struct proto *p,
        const uint8_t *request,
        size_t reqlen)
{
    int result = 0;
    size_t payload_len;
    const struct frame* frame;

    if (!p || !request || reqlen < MIN_REQUEST_LEN)
    {
        return STATUS_ERROR;
    }

    frame = (struct frame*)request;

    p->seq_no = frame->seq_no;

    switch(frame->code) {
        case SIGNAL_SET_RELAY:
            payload_len = reqlen - 2*sizeof(uint8_t);
            result = set_relay_signal(p, frame->payload, payload_len);
            break;
        case SIGNAL_GET_RELAY:
            result = get_relay_signal(p);
            break;
        case SIGNAL_QUIT:
            result = quit_signal(p);
            break;
        default:
            break;
    }

    return result;
}

int proto_ack(struct proto *p, uint8_t *response, size_t maxrsp)
{
    if (!p || !response || maxrsp < 2)
        return STATUS_ERROR;

    response[0] = SIGNAL_ACK;
    response[1] = ++p->seq_no;
    return 2;
}

static uint8_t state2bitmap() {
    int i;
    uint8_t bitmap = 0;
    for(i = 0; i < 2; i++) {
        if (proto_api_get_relay_cb(i)) {
            bitmap |= (1<<i);
        }
    }
    return bitmap;
}

int proto_relay_state(struct proto *p, uint8_t *response, size_t maxrsp)
{
    if (!p || !response || maxrsp < 3)
        return STATUS_ERROR;

    response[0] = SIGNAL_RELAY_STATE;
    response[1] = p->seq_no;
    response[2] = state2bitmap();
    return 3;
}

int proto_relay_state_ack(struct proto *p, const uint8_t *request, size_t reqlen)
{
   const struct frame* frame;

   if (!p || !request || reqlen != 2)
        return STATUS_ERROR;

   frame = (struct frame*)request;
   if (frame->code == SIGNAL_ACK && (p->seq_no + 1) == frame->seq_no) {
       return STATUS_ACK;
   }
   return STATUS_NACK;
}
