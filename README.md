[![pipeline status](https://gitlab.com/arturmadrzak/c-test/badges/master/pipeline.svg)](https://gitlab.com/arturmadrzak/c-test/commits/master)

## License
Copyright (c) 2020 Artur Mądrzak <artur@madrzak.eu>  
The **c-test** is available under the MIT License.

Powered by https://cgreen-devs.github.io
