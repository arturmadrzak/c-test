# c-template project (https://gitlab.com/arturmadrzak/c-template)
# Copyright (c) 2020 Artur Mądrzak <artur@madrzak.eu>

SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)
DEPS = $(SRCS:.c=.d)

CFLAGS = -Wall -fPIC -MMD

.PHONY: all lib test clean

all: main test

main: $(OBJS)

lib: libapp.a

test: lib
	make -C tests

libapp.a: $(OBJS)
	$(AR) csr $@ $^

clean:
	make -C tests clean
	-rm -f *.o
	-rm -f *.d
	-rm -f libapp.a
	-rm -f main

include $(wildcard *.d)
