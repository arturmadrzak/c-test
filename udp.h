#ifndef _UDP_H
#define _UDP_H

#include <sys/socket.h>
#include <sys/types.h>

int udp_open(void);
ssize_t udp_read(int sockfd, void *buf, size_t count, struct sockaddr *src_addr, socklen_t *addrlen);
ssize_t udp_write(int fd, const void *buf, size_t len, const struct sockaddr *dest_addr, socklen_t addrlen);
void udp_close(int fd);

#endif /* _UDP_H */
